#!/bin/bash

set -e

JAVA_HOME=/usr/share/java/jdk-11.0.2
PATH=`echo "${PATH}" |  awk -v RS=: -v ORS=: '/jdk/ {next} {print}'`
PATH="${PATH}:${JAVA_HOME}/bin"

PATHS=".gitignore .gitmodules LICENSE README.md
  appveyor.yml  .travis.yml
  build.gradle gradle gradle.properties gradlew gradlew.bat settings.gradle
  misc modules"

EXCLUDE=

[ -d .git ] && rm -rvf .git
[ -d purplejs-master ] && rm -rvf purplejs-master
for path in $PATHS
do
  [ -e "./${path}" ] && rm -rvf "${path}"
done

if [ -f ~/Downloads/purplejs-master.zip ]
then
  unzip ~/Downloads/purplejs-master.zip
  for path in $PATHS 
  do
    mv -v "./purplejs-master/${path}" .
  done
  rmdir purplejs-master
else
  git init
  git remote add origin https://github.com/purplejs/purplejs.git
  git pull
fi

for path in $EXCLUDE
do
  rm -rvf "${path}"
done

mkdir -pv ./modules/purplejs-gradle/buildSrc
mv -v ./modules/purplejs-gradle/{,buildSrc/}src

patch -Np0 -i ./allow-jdk-11.patch
patch -Np0 -i ./update-deprecated-paths.patch
patch -Np0 -i ./update-gradle-version.patch

if [ "`which gradle 2>/dev/null`x" != "x" ]
then
  gradle wrapper --gradle-version=5.1.1 build --stacktrace
else
  ./gradlew build --stacktrace
fi
