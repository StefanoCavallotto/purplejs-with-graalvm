package io.purplejs.core.internal.value

import com.google.gson.JsonNull
import io.purplejs.core.internal.runtime.Runtime
import io.purplejs.core.internal.runtime.RuntimeFactory
import jdk.nashorn.internal.runtime.ScriptRuntime
import org.graalvm.polyglot.Value
import spock.lang.Specification

import javax.script.ScriptEngine
import java.text.SimpleDateFormat
import java.util.function.Function

class ValueFactoryImplTest
    extends Specification
{
    ScriptEngine engine

    def setup()
    {
        final Runtime runtime = new RuntimeFactory().newRuntime( getClass().getClassLoader() )
        this.engine = runtime.getEngine()
    }

    def "newValue of null"()
    {
        when:
        def value = RuntimeFactory.context.asValue( null )

        then:
        value.isNull()
    }

    def "newValue from scalar"()
    {
        when:
        def value = RuntimeFactory.context.asValue( "2" )

        then:
        !value.isNull()
        !value.hasArrayElements()
        !value.canExecute()
        !value.canInstantiate()
        value.isNumber()

        value.asString() == "2"
        value.asInt() == 2

        assertNonArray( value )
        assertNonObject( value )
        assertNonFunction( value )
    }

    def "newValue of undefined"()
    {
        when:
        def value = RuntimeFactory.context.asValue( ScriptRuntime.UNDEFINED )

        then:
        value.isNull()
    }

    def "newValue from function"()
    {
        when:
        def result = execute( "var result = function(a, b) { return a + b; }; result;" )
        def value = RuntimeFactory.context.asValue( result )

        then:
        !value.isNull()
        !value.hasArrayElements()
        value.canExecute()

        assertNonValue( value )
        assertNonArray( value )
        assertNonObject( value )

        JsonNull.INSTANCE == value.toJson()

        when:
        result = value.call( 10, 11 )

        then:
        result != null
        (result as Double) == 21.0

        when:
        def func = (Function<Object[], Object>) value.toJavaObject()
        result = func.apply( [10, 11].toArray() )

        then:
        result != null
        result == 21.0
    }

    def "newValue from array"()
    {
        when:
        def result = execute( "var result = ['1', '2', undefined]; result;" )
        def value = RuntimeFactory.context.asValue( result )

        then:
        !value.isNull()
        value.hasArrayElements()
        !value.canExecute()
        !value.canInstantiate()

        assertNonValue( value )
        assertNonObject( value )
        assertNonFunction( value )

        value.hasArrayElements()
        value.getArraySize() == 2
        value.getArrayElement( 0 ).asString() == "1"
        value.getArrayElement( 1 ).asString() == "2"
    }

    def "newValue from object"()
    {
        when:
        def result = execute( "var result = {'a':1, 'b':2}; result;" )
        def value = RuntimeFactory.context.asValue( result )

        then:
        !value.isNull()
        !value.hasArrayElements()
        !value.canExecute()
        !value.canInstantiate()
        value.hasMembers()

        assertNonArray( value )
        assertNonFunction( value )

        value.getMemberKeys() != null
        value.getMemberKeys().size() == 2
        value.getMemberKeys().toString() == '[a, b]'

        value.getMember( "a" ) != null
        value.getMember( "a" ).getValue() == 1
        value.hasMember( "a" )
    }

    def "newValue from date"()
    {
        setup:
        def format = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssZ" )
        format.setTimeZone( TimeZone.getTimeZone( "UTC" ) )

        when:
        def result = execute( "var result = new Date(Date.parse('1995-11-12T22:24:25Z')); result;" )
        def value = this.factory.newValue( result )

        then:
        value != null
        !value.isArray()
        !value.isFunction()
        !value.isObject()
        value.isValue()

        when:
        def date = (Date) value.getValue()

        then:
        "1995-11-12T22:24:25+0000" == format.format( date )
    }

    private Object execute( final String script )
    {
        return this.engine.eval( script )
    }

    private static void assertNonValue( final Value value )
    {
        assert value.isNull()
        assert value.as( Integer.class ) == null
    }

    private static void assertNonArray( final Value value )
    {
        assert value.hasArrayElements()
        assert value.getArraySize() == 0
    }

    private static void assertNonObject( final Value value )
    {
        assert value.getMemberKeys() != null
        assert value.getMemberKeys().size() == 0
        assert value.as(Map.class).isEmpty()

        assert value.getMember( "test" ) == null
        assert !value.hasMember( "test" )
    }

    private static void assertNonFunction( final Value value )
    {
        assert value.execute( "a", "b" ) == null
    }
}