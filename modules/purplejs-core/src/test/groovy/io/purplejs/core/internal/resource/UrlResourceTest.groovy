package io.purplejs.core.internal.resource

import io.purplejs.core.resource.ResourcePath
import io.purplejs.core.util.IOHelper

import org.junit.Rule
import org.junit.rules.TemporaryFolder

class UrlResourceTest
    extends ResourceTestSupport
{
    // jessicadharmapennell@gmail.com 2021-02-15T18:24:06-07:00
    // Newer groovy will not allow writeFile to be called from a static
    // context
    @Rule
    def TemporaryFolder temporaryFolder = new TemporaryFolder();

    def "accessors"()
    {
        setup:
        // jessicadharmapennell@gmail.com 2021-02-15T18:24:06-07:00
        // Newer groovy will not allow writeFile to be called from a static
        // context
        //
        // def file = writeFile( 'test.txt', 'hello' );
        def file = temporaryFolder.newFile( 'test.txt' );
        def path = ResourcePath.from( '/a/b/test.txt' );
        // jessicadharmapennell@gmail.com 2021-02-15T18:24:06-07:00
        // Newer groovy will not allow writeFile to be called from a static
        // context
        file << 'hello';

        when:
        def resource = new UrlResource( path, file.toURI().toURL() );

        then:
        resource.path == path;
        resource.size == file.length();
        resource.lastModified == file.lastModified();
        resource.bytes != null;
        IOHelper.readString( resource.bytes ) == 'hello';
    }

    def "url problem"()
    {
        setup:
        def path = ResourcePath.from( '/a/b/test.txt' );

        when:
        def resource = new UrlResource( path, new URL( 'file:not-found' ) );

        then:
        resource.size == -1;
        resource.lastModified == -1;
    }
}
