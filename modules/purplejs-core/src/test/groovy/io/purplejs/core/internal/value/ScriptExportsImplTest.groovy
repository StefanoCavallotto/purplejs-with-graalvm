package io.purplejs.core.internal.value

import io.purplejs.core.internal.runtime.RuntimeFactory
import io.purplejs.core.resource.ResourcePath
import io.purplejs.core.value.ScriptExports
import org.graalvm.polyglot.Value
import spock.lang.Specification

class ScriptExportsImplTest
    extends Specification
{
    ResourcePath resource

    Value value

    ScriptExports exports

    Value callResult

    def setup()
    {
        RuntimeFactory rtf = new RuntimeFactory()
        rtf.newRuntime()
        this.resource = ResourcePath.from( "/a/b" )
        this.value = rtf.context.eval("js", "(class{exists;number;})")
        this.value.putMember("exists", rtf.context.eval("js", "(function(){})"))
        this.value.putMember("number", rtf.context.eval("js", "(function(){return 1;})"))

        this.callResult = rtf.context.asValue(1)
        this.exports = new ScriptExportsImpl( this.resource, this.value )
    }

    def "getResource"()
    {
        when:
        def actual = this.exports.getResource()

        then:
        this.resource == actual
    }

    def "getValue"()
    {
        when:
        def actual = this.exports.getValue()

        then:
        this.value == actual
    }

    def "hasMethod"()
    {
        when:
        def result = this.exports.hasMethod( 'notExists' )

        then:
        !result

        when:
        result = this.exports.hasMethod( 'exists' )

        then:
        result
    }

    def "executeMethod"()
    {
        when:
        def result = this.exports.executeMethod( 'notExists' )

        then:
        result == null

        when:
        result = this.exports.executeMethod( 'exists' )

        then:
        (result != null) && result.isNull()

        when:
        result = this.exports.executeMethod( 'number' )

        then:
        this.callResult == result
    }
}