package io.purplejs.core.internal;

import java.io.File;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import io.purplejs.core.Engine;
import io.purplejs.core.internal.executor.ScriptExecutorImpl;
import io.purplejs.core.internal.runtime.Runtime;
import io.purplejs.core.internal.runtime.RuntimeFactory;
import io.purplejs.core.registry.Registry;
import io.purplejs.core.resource.ResourceLoader;
import io.purplejs.core.resource.ResourcePath;
import io.purplejs.core.settings.Settings;
import io.purplejs.core.value.ScriptExports;

final class EngineImpl
    implements Engine
{
    ResourceLoader resourceLoader;

    ClassLoader classLoader;

    ImmutableMap<String, String> config;

    ImmutableMap<String, Object> globalVariables;

    Registry registry;

    CompositeModule module;

    Settings settings;

    ImmutableList<File> devSourceDirs;

    private final ScriptExecutorImpl executor;

    EngineImpl()
    {
        this.executor = new ScriptExecutorImpl();
    }

    @Override
    public ResourceLoader getResourceLoader()
    {
        return this.resourceLoader;
    }

    @Override
    public ClassLoader getClassLoader()
    {
        return this.classLoader;
    }

    @Override
    public Settings getSettings()
    {
        return this.settings;
    }

    @Override
    public <T> T getInstance( final Class<T> type )
    {
        return this.registry.getInstance( type );
    }

    @Override
    public <T> T getInstanceOrNull( final Class<T> type )
    {
        return this.registry.getInstanceOrNull( type );
    }

    @Override
    public <T> Supplier<T> getProvider( final Class<T> type )
    {
        return this.registry.getProvider( type );
    }

    @Override
    public ScriptExports require( final ResourcePath path )
    {
        return this.executor.executeMain( path );
    }

    @Override
    public List<File> getDevSourceDirs()
    {
        return this.devSourceDirs;
    }

    @Override
    public void dispose()
    {
        this.module.dispose( this );
        this.executor.dispose();
    }

    void init()
    {
        final RuntimeFactory runtimeFactory = new RuntimeFactory();
        final Runtime runtime = runtimeFactory.newRuntime();

        this.executor.setEnvironment( this );
        this.executor.setNashornRuntime(runtime);
        this.executor.init();

        this.module.init( this );
    }
}
