package io.purplejs.core.internal.json;

import io.purplejs.core.internal.runtime.JSHelper;
import io.purplejs.core.internal.runtime.Runtime;

public final class ScriptJsonGenerator
    extends AbstractJsonGenerator
{
    private final Runtime runtime;

    public ScriptJsonGenerator( final Runtime runtime )
    {
        this.runtime = runtime;
    }

    @Override
    protected Object newMap()
    {
        return this.runtime.newJsObject();
    }

    @Override
    protected Object newArray()
    {
        return this.runtime.newJsArray();
    }

    @Override
    protected boolean isMap( final Object value )
    {
        return JSHelper.isObjectType( value );
    }

    @Override
    protected boolean isArray( final Object value )
    {
        return JSHelper.isArrayType( value );
    }

    @Override
    protected void putInMap( final Object map, final String key, final Object value )
    {
        if ( value != null )
        {
            JSHelper.addToObject( map, key, value );
        }
    }

    @Override
    protected void addToArray( final Object array, final Object value )
    {
        JSHelper.addToArray( array, value );
    }

    @Override
    protected AbstractJsonGenerator newGenerator()
    {
        return new ScriptJsonGenerator( this.runtime );
    }
}
