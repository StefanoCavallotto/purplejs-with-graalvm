package io.purplejs.core.graalvm;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.object.Shape;
import com.oracle.truffle.js.runtime.builtins.JSFunctionObject;
import com.oracle.truffle.js.runtime.interop.InteropBoundFunction;
import com.oracle.truffle.object.PropertyMap;
import com.oracle.truffle.object.ShapeBasic;
import io.purplejs.core.internal.compat.CompatScriptObjectMirror;
import io.purplejs.core.internal.runtime.RuntimeFactory;
import org.apache.commons.lang3.StringUtils;
import org.graalvm.collections.Pair;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

@SuppressWarnings({"JavaReflectionMemberAccess", "unused", "unchecked", "AccessStaticViaInstance"})
public class GraalVMSOM implements CompatScriptObjectMirror<Pair<Object, Value>>
{
    /** Reflection from this mirror on Java */
    public final Object myHostObject;

    /** Reflection from this mirror on JavaScript */
    public final Value myReflection;

    /** Backing memory for all ScriptObjectMirrors */
    protected static ScriptObjectContext mem = new ScriptObjectContext();

    /** Unique name in JavaScript for this object */
    public final String jsName;

    /** Previously boxed objects */
    private static final Map<Value, Object> boxCache = new HashMap<>();

    /** All types for a boxed reflection object */
    public enum HostObjectType
    {
        NULL, UNDEFINED,
        EXCEPTION, FUNCTION,
        LIST, PROXYOBJECT, BUFFER, ITERATOR,
        NUMBER, BOOLEAN, STRING,
        DATE, TIME, TIMEZONE, DURATION;

        /** Reflection decoder */
        public static HostObjectType fromReflection(Value reflection)
        {
            Method hasBufferElements;

            if (reflection == null || reflection.isNull())
            {
                return NULL;
            }
            if (reflection.isException())
            {
                return EXCEPTION;
            }
            if (reflection.isMetaObject() || reflection.canInstantiate() || reflection.canExecute())
            {
                return FUNCTION;
            }
            if (reflection.hasArrayElements())
            {
                return LIST;
            }
            if (reflection.hasMembers() || reflection.isProxyObject())
            {
                return PROXYOBJECT;
            }
            // Note: Introduced in GraalVM-js 21.1
            try
            {
                hasBufferElements = Value.class.getMethod("hasBufferElements");
                if ((Boolean) hasBufferElements.invoke(reflection))
                {
                    return BUFFER;
                }
            }
            catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | UnsupportedOperationException ignored) { }
            // Note : do after array, object, etc as these are iterators too
            try
            {
                if (reflection.as(Iterator.class) != null)
                {
                    return ITERATOR;
                }
            }
            catch (ClassCastException | PolyglotException | IllegalStateException | NullPointerException ignored) {}
            if (reflection.isNumber())
            {
                return NUMBER;
            }
            if (reflection.isBoolean())
            {
                return BOOLEAN;
            }
            if (reflection.isDate())
            {
                return DATE;
            }
            if (reflection.isTimeZone())
            {
                return TIMEZONE;
            }
            if (reflection.isDuration())
            {
                return DURATION;
            }
            if (reflection.isTime())
            {
                return TIME;
            }
            if (reflection.isString())
            {
                return STRING;
            }

            return UNDEFINED;
        }

        /** Host object decoder */
        public static HostObjectType fromHostObject(Object hostObject)
        {
            if (hostObject == null)
            {
                return NULL;
            }
            if ( (hostObject instanceof Value) && ((Value) hostObject).isNull() )
            {
                return NULL;
            }
            if (hostObject instanceof Exception)
            {
                return EXCEPTION;
            }
            if (hostObject instanceof CompatBufferElementCollection)
            {
                return BUFFER;
            }
            if (hostObject instanceof JSFunctionObjectWithMetadata)
            {
                return FUNCTION;
            }
            if (hostObject instanceof List)
            {
                return LIST;
            }
            if (hostObject instanceof Iterator)
            {
                return ITERATOR;
            }
            if (hostObject instanceof Number)
            {
                return NUMBER;
            }
            if (hostObject instanceof Boolean)
            {
                return BOOLEAN;
            }
            if (hostObject instanceof Date)
            {
                return DATE;
            }
            if (hostObject instanceof TimeZone)
            {
                return TIMEZONE;
            }
            if (hostObject instanceof Duration)
            {
                return DURATION;
            }
            if (hostObject instanceof LocalTime)
            {
                return TIME;
            }
            if (hostObject instanceof String)
            {
                return STRING;
            }
            if (hostObject instanceof ProxyObject)
            {
                return PROXYOBJECT;
            }
            return UNDEFINED;
        }

        /** @return a simple name accessible to both JS and Java */
        public static String getSimpleName(HostObjectType type, String defaultValue)
        {
            switch (type)
            {
                case LIST :
                    return ArrayList.class.getSimpleName();
                case BUFFER :
                    return CompatBufferElementCollection.class.getSimpleName();
                case ITERATOR :
                    return Iterator.class.getSimpleName();
                case PROXYOBJECT :
                    if (getGraalVMBindings().hasMember(defaultValue))
                    {
                        return defaultValue;
                    }
                    return Object.class.getSimpleName();
                default :
                    return defaultValue;
            }
        }
        public static String getSimpleName(Object hostObject, String defaultValue)
        {
            if (defaultValue == null || defaultValue.isBlank())
            {
                //noinspection SwitchStatementWithTooFewBranches
                switch (fromHostObject(hostObject))
                {
                    case FUNCTION :
                        defaultValue = hostObject.toString();
                        break;
                    default :
                        defaultValue = hostObject.getClass().getSimpleName();;
                        break;
                }
            }
            return getSimpleName(fromHostObject(hostObject), defaultValue);
        }
        public static String getSimpleName(Object hostObject)
        {
            return getSimpleName(hostObject, "");
        }
        public static String getSimpleName(Value reflection, String defaultValue)
        {
            if (defaultValue == null || defaultValue.isBlank())
            {
                //noinspection SwitchStatementWithTooFewBranches
                switch (fromReflection(reflection))
                {
                    case FUNCTION :
                        defaultValue = boxReflection(reflection).toString();
                        break;
                    default :
                        defaultValue = boxReflection(reflection).getClass().getSimpleName();
                }
            }
            return getSimpleName(fromReflection(reflection), defaultValue);
        }

        /** @return a canon name name accessible to both JS and Java */
        public static String getCanonicalName(HostObjectType type, String defaultValue)
        {
            switch (type)
            {
                case LIST :
                    return ArrayList.class.getCanonicalName();
                case BUFFER :
                    return CompatBufferElementCollection.class.getCanonicalName();
                case ITERATOR :
                    return Iterator.class.getCanonicalName();
                case PROXYOBJECT :
                    if (getGraalVMBindings().hasMember(defaultValue))
                    {
                        return defaultValue;
                    }
                    return Object.class.getCanonicalName();
                default :
                    return defaultValue;
            }
        }
        public static String getCanonicalName(Object hostObject, String defaultValue)
        {
            if (defaultValue == null || defaultValue.isBlank())
            {
                //noinspection SwitchStatementWithTooFewBranches
                switch (fromHostObject(hostObject))
                {
                    case FUNCTION :
                        defaultValue = hostObject.toString();
                        break;
                    default :
                        defaultValue = hostObject.getClass().getCanonicalName();;
                        break;
                }
            }
            return getCanonicalName(fromHostObject(hostObject), defaultValue);
        }
        public static String getCanonicalName(Object hostObject)
        {
            return getCanonicalName(hostObject, "");
        }
        public static String getCanonicalName(Value reflection, String defaultValue)
        {
            if (defaultValue == null || defaultValue.isBlank())
            {
                //noinspection SwitchStatementWithTooFewBranches
                switch (fromReflection(reflection))
                {
                    case FUNCTION :
                        defaultValue = boxReflection(reflection).toString();
                        break;
                    default :
                        defaultValue = boxReflection(reflection).getClass().getCanonicalName();
                }
            }
            return getCanonicalName(fromReflection(reflection), defaultValue);
        }
    }

    /** Allows easy access to all appropriate Nashorn and GraalVM bindings */
    public static class ScriptObjectContext implements Map<String, GraalVMSOM>
    {
        /** Map used to synchronize Nashorn and GraalVM bindings */
        public static final Map<String, GraalVMSOM> memory = new HashMap<>();

        /** Reference to GraalVM bindings */
        @NotNull
        public Value getBindings()
        {
            return getContext().getBindings(RuntimeFactory.JAVASCRIPT_EXT);
        }

        /** Script execution context */
        @NotNull
        public Context getContext()
        {
            return Context.getCurrent();
        }

        /** Use this to generate eval scripts */
        @NotNull
        public static String getExportJavaTypeName(@NotNull Object hostObject)
        {
            String simpleName = HostObjectType.getSimpleName(hostObject);
            String canonName = HostObjectType.getCanonicalName(hostObject);
            return String.format("var %s = Java.type('%s');%s", simpleName, canonName, simpleName);
        }

        /** Use this to generate JavaScript names from host created values */
        @NotNull
        public static String getNewJSObjectName(@NotNull Object hostObject)
        {
            if (hostObject.hashCode() != 0)
            {
                return HostObjectType.getSimpleName(hostObject) + Math.abs(hostObject.hashCode());
            }
            return HostObjectType.getSimpleName(hostObject) + UUID.randomUUID().toString().replaceAll("-", "");
        }

        /** Use this to generate JavaScript for new object instances */
        @NotNull
        public static String getNewInstanceJS(@NotNull String type, String name, @NotNull Object hostObject, Object... argv)
        {
            String nameFmt = "%s%s";
            String fmt = "var %s = new %s(%s);%s";
            String hashCode = hostObject.hashCode() == 0
                    ? UUID.randomUUID().toString().replaceAll("-", "")
                    : Integer.toString(Math.abs(hostObject.hashCode()));
            String args = "";

            if (name == null || name.isBlank())
            {
                name = String.format(nameFmt, type, hashCode);
            }
            for (Object arg : argv)
            {
                args = String.join(",", args, arg.toString());
            }

            return String.format(fmt, name, type, args, name);
        }

        @Override
        public int size()
        {
            return memory.size();
        }

        @Override
        public boolean isEmpty()
        {
            return memory.isEmpty();
        }

        @Override
        public boolean containsKey(Object key)
        {
            return containsKey(key.toString());
        }
        public boolean containsKey(String key)
        {
            return memory.containsKey(key);
        }

        @Override
        public boolean containsValue(Object value)
        {
            if (value instanceof GraalVMSOM)
            {
                return containsValue((GraalVMSOM) value);
            }
            for (GraalVMSOM mvalue : mem.values())
            {
                if (mvalue.equals(value))
                {
                    return true;
                }
            }
            return false;
        }
        public boolean containsValue(Value value)
        {
            for (GraalVMSOM mvalue : mem.values())
            {
                if (mvalue.equals(value))
                {
                    return true;
                }
            }
            return false;
        }
        public boolean containsValue(GraalVMSOM value)
        {
            return memory.containsValue(value);
        }
        public boolean containsValue(Pair<Object, Value> value)
        {
            return memory.containsValue(new GraalVMSOM(value));
        }

        @Override @NotNull
        public GraalVMSOM get(Object key)
        {
            return get(key.toString());
        }
        @NotNull
        public GraalVMSOM get(String key)
        {
            if (! memory.containsKey(key))
            {
                return getUndefinedValue();
            }
            return memory.get(key);
        }

        @NotNull
        public GraalVMSOM put(Object key, GraalVMSOM value)
        {
            return put(key.toString(), value);
        }
        @Override @NotNull
        public GraalVMSOM put(String key, GraalVMSOM value)
        {
            GraalVMSOM frame = getUndefinedValue();

            if (containsKey(key))
            {
                frame = get(key);
            }

            memory.put(key, value);
            if (! this.getBindings().hasMember(key))
            {
                this.getBindings().putMember(key, value.myReflection);
            }

            return frame;
        }
        @NotNull
        public GraalVMSOM put(String key, Object value)
        {
            Value reflection;
            Object box;

            // already reflected in JavaScript
            if (this.getBindings().hasMember(key))
            {
                reflection = this.getBindings().getMember(key);
                box = boxReflection(reflection);
                memory.put(key, null);
                memory.put(key, new GraalVMSOM(key, box, reflection));
            }
            // not yet reflected in JavaScript, make a new one
            else
            {
                //noinspection RedundantArrayCreation
                memory.put(key, GraalVMSOM.create(value, key, true, new Object[0]));
                this.getBindings().putMember(key, memory.get(key).myReflection);
            }

            return memory.get(key);
        }
        @NotNull
        public GraalVMSOM put(String key, Value value)
        {
            return put(key, boxReflection(value));
        }

        @Override @NotNull
        public GraalVMSOM remove(Object key)
        {
            return remove(key.toString());
        }
        @NotNull
        public GraalVMSOM remove(String key)
        {
            GraalVMSOM frame = getUndefinedValue();

            if (getBindings().hasMember(key))
            {
                getBindings().removeMember(key);
            }
            if (memory.containsKey(key))
            {
                frame = memory.get(key);
            }

            return frame;
        }

        @Override
        public void putAll(Map<? extends String, ? extends GraalVMSOM> m)
        {
            for (Entry<? extends String, ? extends GraalVMSOM> row : m.entrySet())
            {
                put(row.getKey(), row.getValue());
            }
        }

        @Override
        public void clear()
        {
            for (Entry<String, GraalVMSOM> row : memory.entrySet())
            {
                remove(row.getKey());
            }
        }

        @Override @NotNull
        public Set<String> keySet() {
            return memory.keySet();
        }

        @Override @NotNull
        public Collection<GraalVMSOM> values() {
            return memory.values();
        }

        @Override @NotNull
        public Set<Entry<String, GraalVMSOM>> entrySet() {
            return memory.entrySet();
        }
    }

    /** Base constructor */
    public GraalVMSOM(String name, Object hostObject, Value reflection)
    {
        this.myHostObject = hostObject;
        this.myReflection = reflection;
        this.jsName = name;

        if ( (! name.equals("(undefined)")) && (! mem.containsKey(name)) )
        {
            mem.put(this.jsName, this);
        }
    }
    public GraalVMSOM(String name, Pair<Object, Value> mirror)
    {
        this(name, mirror.getLeft(), mirror.getRight());
    }

    /** Constructor, gives this mirror object a derived name, does not eval in JS */
    public GraalVMSOM(Object hostObject, Value reflection)
    {
        this(ScriptObjectContext.getNewJSObjectName(hostObject), hostObject, reflection);
    }
    public GraalVMSOM(Pair<Object, Value> mirror)
    {
        this(ScriptObjectContext.getNewJSObjectName(mirror.getLeft()), mirror);
    }

    /** Get the undefined object (no binding) */
    public static GraalVMSOM getUndefinedValue()
    {
        Value v;
        Pair<Object, Value> mirror;
        GraalVMSOM frame;

        if (mem.containsKey("(undefined)"))
        {
            return mem.get("(undefined)");
        }

        v = GraalVMSOM.getContext().asValue(null);
        mirror = Pair.create(null, v);
        frame = new GraalVMSOM("(undefined)", mirror);
        ScriptObjectContext.memory.put("(undefined)", frame);

        return frame;
    }

    /**
     * Create a ScriptObjectMirror from an object.
     * This is always reflected in Java and Javascript.
     *
     * @param toBox An Object, Value, or ProxyObject reflected in Java.
     *              If not already, this will be reflected in JavaScript.
     *              New JavaScript reflections are always made for Objects.
     *              ProxyObjects are treated as Map&lt;String, Object&gt;.
     * @param newInstArgs Arguments to the constructor if this is a new object
     * @return a ScriptMirrorObject reflected in Java and JavaScript
     */
    /////////////////
    // Host Object //
    /////////////////
    public static GraalVMSOM create(Object toBox, String name, boolean makeInstance, Object... newInstArgs)
    {
        String protoJS, newJS, simpleName;
        Object protoBox, newBox;
        Value proto, reflection;
        JSFunctionObjectWithMetadata func;
        GraalVMSOM protoframe, checkIfChanged;

        // already prepped
        if (toBox instanceof GraalVMSOM)
        {
            if ( (mem.containsValue((GraalVMSOM) toBox)) && (! makeInstance) )
            {
                return ((GraalVMSOM) toBox);
            }
            toBox = ((GraalVMSOM) toBox).myHostObject;
        }

        // reflected value passed in
        if (toBox instanceof Value)
        {
            if ( (name != null) && (! name.isBlank()) && (mem.containsKey(name)) )
            {
                checkIfChanged = mem.get(name);
                if (checkIfChanged.myHostObject == toBox)
                {
                    return checkIfChanged;
                }
            }
            toBox = boxReflection((Value) toBox);
        }

        // function passed in
        if (toBox instanceof JSFunctionObjectWithMetadata)
        {
            func = (JSFunctionObjectWithMetadata) toBox;
            if (name == null || name.isBlank())
            {
                name = func.name();
            }
            if (func.source == null || func.source.isEmpty())
            {
                func.source = "(builtin) " + name;
                func.sourceArgc = newInstArgs.length;
                func.sourceArgv = newInstArgs;
            }
        }

        // get default name
        if (name == null || name.isBlank())
        {
            name = ScriptObjectContext.getNewJSObjectName(toBox);
        }

        // add to memory if not yet present
        if (! mem.containsKey(name))
        {
            // JS side class name
            simpleName = HostObjectType.getSimpleName(toBox, name);

            // not a built-in
            if (! mem.getBindings().hasMember(name))
            {
                // get prototype for making this
                if (! mem.containsKey(simpleName))
                {
                    protoJS = ScriptObjectContext.getExportJavaTypeName(toBox);
                    proto = getContext().eval(RuntimeFactory.JAVASCRIPT_EXT, protoJS);
                    protoBox = boxReflection(proto);
                    // loads prototype into memory/bindings automatically
                    protoframe = new GraalVMSOM(simpleName, protoBox, proto);
                }
                else
                {
                    protoframe = mem.get(simpleName);
                }
            }
            // built-ins are in bindings but not necessarily memory
            else
            {
                if (! mem.containsKey(simpleName))
                {
                    mem.put(name, mem.getBindings().getMember(name));
                }
                protoframe = mem.get(simpleName);
            }

            // prototype definition requested
            if (! makeInstance)
            {
                return protoframe;
            }

            // instance requested
            newJS = ScriptObjectContext.getNewInstanceJS(simpleName, name, toBox, newInstArgs);
            reflection = getContext().eval(RuntimeFactory.JAVASCRIPT_EXT, newJS);
            newBox = boxReflection(reflection);
            return new GraalVMSOM(name, newBox, reflection);
        }

        return mem.get(name);
    }
    // needed for erasure
    public static GraalVMSOM create(String toBox, String name, boolean makeInstance, Object[] newInstArgs)
    {
        //noinspection UnnecessaryLocalVariable
        Object toBoxAsObj = toBox;
        return create(toBoxAsObj, name, makeInstance, newInstArgs);
    }
    // default : make prototype, not instance
    public static GraalVMSOM create(Object toBox, String name, Object... newInstArgs)
    {
        return create(toBox, name, false, newInstArgs);
    }
    // needed for erasure
    public static GraalVMSOM create(String toBox, String name, Object[] newInstArgs)
    {
        //noinspection UnnecessaryLocalVariable
        Object toBoxAsObj = toBox;
        return create(toBoxAsObj, name, false, newInstArgs);
    }
    // default name : derived from toBox
    public static GraalVMSOM create(Object toBox, boolean makeInstance, Object... newInstArgs)
    {
        return create(toBox, "", makeInstance, newInstArgs);
    }
    // needed for erasure
    public static GraalVMSOM create(String toBox, boolean makeInstance, Object[] newInstArgs)
    {
        //noinspection UnnecessaryLocalVariable
        Object toBoxAsObj = toBox;
        return create(toBoxAsObj, "", makeInstance, newInstArgs);
    }
    // default name, make prototype
    public static GraalVMSOM create(Object toBox, Object[] newInstArgs)
    {
        return create(toBox, false, newInstArgs);
    }
    // needed for erasure
    public static GraalVMSOM create(String toBox, Object[] newInstArgs)
    {
        //noinspection UnnecessaryLocalVariable
        Object toBoxAsObj = toBox;
        return create(toBoxAsObj, "", false, newInstArgs);
    }
    //////////////////////////////////////
    // Reflected Value, value arguments //
    //////////////////////////////////////
    public static GraalVMSOM create(Value toBox, String name, boolean makeInstance, Value... newInstArgs)
    {
        Object[] args = new Object[newInstArgs.length];
        int i = 0;

        for (Value v : newInstArgs)
        {
            args[i] = boxReflection(newInstArgs[i]);
            ++i;
        }
        return create(boxReflection(toBox), name, makeInstance, args);
    }
    // default : make prototype, not instance
    public static GraalVMSOM create(Value toBox, String name, Value... newInstArgs)
    {
        return create(toBox, name, false, newInstArgs);
    }
    // default name : derived from toBox
    public static GraalVMSOM create(Value toBox, boolean makeInstance, Value... newInstArgs)
    {
        return create(toBox, "", makeInstance, newInstArgs);
    }
    // default name, make prototype
    public static GraalVMSOM create(Value toBox, Value[] newInstArgs)
    {
        return create(toBox, false, newInstArgs);
    }
    ///////////////////////////////////////
    // Reflected value, object arguments //
    ///////////////////////////////////////
    public static GraalVMSOM create(Value toBox, String name, boolean makeInstance, Object... newInstArgs)
    {
        return create(boxReflection(toBox), name, makeInstance, newInstArgs);
    }
    // default : make prototype, not instance
    public static GraalVMSOM create(Value toBox, String name, Object... newInstArgs)
    {
        return create(boxReflection(toBox), name, false, newInstArgs);
    }
    // default name : derived from toBox
    public static GraalVMSOM create(Value toBox, boolean makeInstance, Object... newInstArgs)
    {
        return create(toBox, "", makeInstance, newInstArgs);
    }
    // default name, make prototype
    public static GraalVMSOM create(Value toBox, Object[] newInstArgs)
    {
        return create(toBox, false, newInstArgs);
    }

    /** Prep an executable Value */
    public static GraalVMSOM prepExecutable(String source, Object... argv)
    {
        int argc = argv.length;
        Value reflection;
        JSFunctionObjectWithMetadata box;
        String jsName = "func" + UUID.randomUUID().toString().replaceAll("-", "");

        source = StringUtils.stripStart(source, "(");
        source = StringUtils.stripEnd(source, ")");
        source = String.format("var %s = %s;%s", jsName, source, jsName);
        reflection = getContext().eval( RuntimeFactory.JAVASCRIPT_EXT, source );
        box = (JSFunctionObjectWithMetadata) boxReflection(reflection);

        box.sourceArgc = argc;
        box.sourceArgv = argv;
        box.source = source;

        return new GraalVMSOM(jsName, Pair.create(box, reflection));
    }

    /** Resolves https://github.com/oracle/graal/issues/583 */
    public static Set<String> getAllMemberKeysForValue(Value reflection)
    {
        Set<String> keys = new HashSet<>(reflection.getMemberKeys());
        Iterator<Object> getKeys = reflection.as(Iterator.class);
        Method getGuestObject;
        Field shape;
        InvocationHandler h;
        DynamicObject guestObjectD;
        ProxyObject guestObjectP;
        PropertyMap propertyList;
        Value testMember;

        try {
            h = ((Proxy) getKeys).getInvocationHandler(getKeys);
            getGuestObject = h.getClass().getDeclaredMethod("getGuestObject");
            getGuestObject.setAccessible(true);
            if (reflection.isProxyObject())
            {
                guestObjectP = reflection.asProxyObject();
                shape = ProxyObject.class.getDeclaredField("shape");
                shape.setAccessible(true);
                propertyList = ((ShapeBasic) shape.get(guestObjectP)).getPropertyMap();
            }
            else
            {
                guestObjectD = (DynamicObject) getGuestObject.invoke(h);
                shape = DynamicObject.class.getDeclaredField("shape");
                shape.setAccessible(true);
                propertyList = ((ShapeBasic) shape.get(guestObjectD)).getPropertyMap();
            }

            for (Object key : propertyList.keySet())
            {
                if (! keys.contains(key.toString()))
                {
                    testMember = reflection.getMember(key.toString());
                    if (testMember != null)
                    {
                        reflection.removeMember(key.toString());
                        reflection.putMember(key.toString(), testMember);
                        keys.add(key.toString());
                    }
                }
            }
        }
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException ignored) { }

        assert(reflection.getMemberKeys().size() == keys.size());
        return keys;
    }

    /** Box up a reflection when a host is not provided */
    public static Object boxReflection(@NotNull Value reflection)
    {
        Object boxed;

        // host object : already boxed
        if (reflection.isHostObject())
        {
            return reflection.asHostObject();
        }

        // cached object
        if (boxCache.containsKey(reflection))
        {
            return boxCache.get(reflection);
        }

        switch(HostObjectType.fromReflection(reflection))
        {
            // base case
            case NULL :
                return reflection;

            // exception : use basic exception with string
            case EXCEPTION :
                boxed = new Exception(reflection.asString());
                boxCache.put(reflection, boxed);
                return boxed;

            // meta classes and functions : expose JSFunctionObject
            case FUNCTION :
                String methodType = "";
                Field receiverField;
                Object receiver;
                InteropBoundFunction functionWrapper;
                JSFunctionObject function;

                if (reflection.isMetaObject())
                {
                    methodType = reflection.getMetaQualifiedName();
                }
                else if (reflection.isProxyObject() || reflection.isHostObject())
                {
                    methodType = reflection.getClass().getCanonicalName();
                }

                try
                {
                    receiverField = Value.class.getDeclaredField("receiver");
                    receiverField.setAccessible(true);
                    receiver = receiverField.get(reflection);
                    if (receiver instanceof InteropBoundFunction)
                    {
                        functionWrapper = (InteropBoundFunction) receiver;
                        function = functionWrapper.getFunction();
                    }
                    else if (receiver instanceof JSFunctionObject)
                    {
                        function = (JSFunctionObject) receiver;
                    }
                    else
                    {
                        throw new ClassCastException();
                    }
                }
                catch (NoSuchFieldException | IllegalAccessException | ClassCastException e)
                {
                    throw new UnsupportedOperationException("Could not access function : " + reflection.toString());
                }

                if (methodType.isBlank())
                {
                    function.getFunctionData().setName(ScriptObjectContext.getNewJSObjectName(function));
                }
                else
                {
                    function.getFunctionData().setName(methodType);
                }

                boxed = new JSFunctionObjectWithMetadata(function);
                boxCache.put(reflection, boxed);
                return boxed;

            // array : list of objects
            case LIST :
                List<Object> outarr = new ArrayList<>();
                long n = reflection.getArraySize();

                for (long i = 0; i < n; i++)
                {
                    outarr.add(reflection.getArrayElement(i));
                }

                boxed = outarr;
                boxCache.put(reflection, boxed);
                return boxed;

            // BufferedElement (flow through to object if version < 21.1
            case BUFFER :
                try
                {
                    boxed = new CompatBufferElementCollection(reflection);
                    boxCache.put(reflection, boxed);
                    return boxed;
                }
                catch (NoSuchMethodException ignored) { }

            // ordinary object : return a mirror with no frame
            case PROXYOBJECT :
                Map<String, Pair<Object, Value>> retval = new HashMap<>();
                Value memReflection;
                Object memBox;
                Set<String> keys = getAllMemberKeysForValue(reflection);
                Map<String, Object> boxRetval = new HashMap<>();

                for (String key : keys)
                {
                    memReflection = reflection.getMember(key);
                    if (memReflection == null)
                    {
                        continue;
                    }
                    memBox = boxReflection(memReflection);
                    retval.put(key, Pair.create(memBox, memReflection));
                }

                for (String key : retval.keySet())
                {
                    boxRetval.put(key, retval.get(key).getLeft());
                }

                boxed = ProxyObject.fromMap(boxRetval);
                boxCache.put(reflection, boxed);
                return boxed;

            // iterator : Value iterator containing all members
            case ITERATOR :
                List<Value> iteratorValues = new ArrayList<>();
                Value nextValue;

                // try to grab this Value's "official" iterator first
                try
                {
                    boxed = reflection.as(Iterator.class);
                    boxCache.put(reflection, boxed);
                    return boxed;
                }

                // could not instantiate directly, create by hand
                catch (ClassCastException | PolyglotException | IllegalStateException | NullPointerException e)
                {
                    keys = getAllMemberKeysForValue(reflection);

                    for (String key : keys)
                    {
                        nextValue = reflection.getMember(key);
                        if (nextValue == null)
                        {
                            continue;
                        }
                        iteratorValues.add(nextValue);
                    }

                    boxed = iteratorValues.iterator();
                    boxCache.put(reflection, boxed);
                    return boxed;
                }

            // primitives

            case NUMBER :
                double d = reflection.asDouble();
                long l = reflection.asLong();
                if (Math.floor(d) == Math.ceil(d))
                {
                    boxCache.put(reflection, l);
                    return l;
                }
                boxCache.put(reflection, d);
                return d;
            case BOOLEAN :
                boxed = reflection.asBoolean();
                boxCache.put(reflection, boxed);
                return boxed;
            case DATE :
                boxed = reflection.asDate();
                boxCache.put(reflection, boxed);
                return boxed;
            case TIME :
                boxed = reflection.asTime();
                boxCache.put(reflection, boxed);
                return boxed;
            case TIMEZONE :
                boxed = reflection.asTimeZone();
                boxCache.put(reflection, boxed);
                return boxed;
            case DURATION :
                boxed = reflection.asDuration();
                boxCache.put(reflection, boxed);
                return boxed;
            case STRING :
                boxed = reflection.asString();
                boxCache.put(reflection, boxed);
                return boxed;
        }

        throw new UnsupportedOperationException("Could not box value : " + reflection.toString());
    }

    /** Allows setting metadata on a function object, useful for debugging JS errors */
    public static class JSFunctionObjectWithMetadata extends JSFunctionObject
    {
        public int sourceArgc;
        public Object[] sourceArgv;
        public String source;
        public int lastCallArgc;
        public Object[] lastCallArgv;

        public JSFunctionObjectWithMetadata(JSFunctionObject source)
        {
            super (
                    Shape.newBuilder().layout(JSFunctionObject.class).build(),
                    source.getFunctionData(),
                    source.getEnclosingFrame(),
                    source.getRealm(),
                    source.getClassPrototype()
            );
        }

        @Override
        public String toString()
        {
            return source;
        }

        public String name()
        {
            return getFunctionData().getName();
        }
    }

    /** Allows access to BufferElement methods before GraalVM-js 21.1 */
    public static class CompatBufferElementCollection
    {
        /** BufferElement is a kind of Value */
        public final Value reflection;

        private final Method getBufferSizeMethod;
        private final Method isBufferWriteableMethod;
        private final Method readBufferFloatMethod;
        private final Method readBufferDoubleMethod;
        private final Method readBufferByteMethod;
        private final Method readBufferShortMethod;
        private final Method readBufferIntMethod;
        private final Method readBufferLongMethod;
        private final Method writeBufferFloatMethod;
        private final Method writeBufferDoubleMethod;
        private final Method writeBufferByteMethod;
        private final Method writeBufferShortMethod;
        private final Method writeBufferIntMethod;
        private final Method writeBufferLongMethod;

        public CompatBufferElementCollection(Value reflection) throws NoSuchMethodException
        {
            this.reflection = reflection;
            this.getBufferSizeMethod = Value.class.getMethod("getBufferSize");
            this.isBufferWriteableMethod = Value.class.getMethod("isBufferWriteable");

            this.readBufferFloatMethod = Value.class.getMethod("readBufferFloat", ByteOrder.class, Long.class);
            this.readBufferDoubleMethod = Value.class.getMethod("readBufferDouble", ByteOrder.class, Long.class);
            this.readBufferByteMethod = Value.class.getMethod("readBufferByte", Long.class);
            this.readBufferShortMethod = Value.class.getMethod("readBufferShort", ByteOrder.class, Long.class);
            this.readBufferIntMethod = Value.class.getMethod("readBufferInt", ByteOrder.class, Long.class);
            this.readBufferLongMethod = Value.class.getMethod("readBufferLong", ByteOrder.class, Long.class);

            this.writeBufferFloatMethod = Value.class.getMethod("writeBufferFloat", ByteOrder.class, Long.class, Float.class);
            this.writeBufferDoubleMethod = Value.class.getMethod("writeBufferDouble", ByteOrder.class, Long.class, Double.class);
            this.writeBufferByteMethod = Value.class.getMethod("writeBufferByte", Long.class, Byte.class);
            this.writeBufferShortMethod = Value.class.getMethod("writeBufferShort", ByteOrder.class, Long.class, Short.class);
            this.writeBufferIntMethod = Value.class.getMethod("writeBufferInt", ByteOrder.class, Long.class, Integer.class);
            this.writeBufferLongMethod = Value.class.getMethod("writeBufferLong", ByteOrder.class, Long.class, Long.class);
        }

        public long getBufferSize() throws InvocationTargetException, IllegalAccessException
        {
            return (Long) this.getBufferSizeMethod.invoke(reflection);
        }
        public boolean isBufferWriteable() throws InvocationTargetException, IllegalAccessException
        {
            return (Boolean) this.isBufferWriteableMethod.invoke(reflection);
        }
        public float readBufferFloat(ByteOrder order, long offset) throws InvocationTargetException, IllegalAccessException
        {
            return (Float) readBufferFloatMethod.invoke(reflection, order, offset);
        }
        public double readBufferDouble(ByteOrder order, long offset) throws InvocationTargetException, IllegalAccessException
        {
            return (Double) readBufferDoubleMethod.invoke(reflection, order, offset);
        }
        public byte readBufferByte(ByteOrder order, long offset) throws InvocationTargetException, IllegalAccessException
        {
            return (Byte) readBufferByteMethod.invoke(reflection, offset);
        }
        public short readBufferShort(ByteOrder order, long offset) throws InvocationTargetException, IllegalAccessException
        {
            return (Short) readBufferShortMethod.invoke(reflection, order, offset);
        }
        public int readBufferInt(ByteOrder order, long offset) throws InvocationTargetException, IllegalAccessException
        {
            return (Integer) readBufferIntMethod.invoke(reflection, order, offset);
        }
        public long readBufferLong(ByteOrder order, long offset) throws InvocationTargetException, IllegalAccessException
        {
            return (Long) readBufferLongMethod.invoke(reflection, order, offset);
        }

        public float writeBufferFloat(ByteOrder order, long offset, Float value) throws InvocationTargetException, IllegalAccessException
        {
            return (Float) writeBufferFloatMethod.invoke(reflection, order, offset, value);
        }
        public double writeBufferDouble(ByteOrder order, long offset, Double value) throws InvocationTargetException, IllegalAccessException
        {
            return (Double) writeBufferDoubleMethod.invoke(reflection, order, offset, value);
        }
        public byte writeBufferByte(ByteOrder order, long offset, Byte value) throws InvocationTargetException, IllegalAccessException
        {
            return (Byte) writeBufferByteMethod.invoke(reflection, offset, value);
        }
        public short writeBufferShort(ByteOrder order, long offset, Short value) throws InvocationTargetException, IllegalAccessException
        {
            return (Short) writeBufferShortMethod.invoke(reflection, order, offset, value);
        }
        public int writeBufferInt(ByteOrder order, long offset, int value) throws InvocationTargetException, IllegalAccessException
        {
            return (Integer) writeBufferIntMethod.invoke(reflection, order, offset, value);
        }
        public long writeBufferLong(ByteOrder order, long offset, long value) throws InvocationTargetException, IllegalAccessException
        {
            return (Long) writeBufferLongMethod.invoke(reflection, order, offset, value);
        }
    }

    /** Get GraalVM Execution Context for this object */
    public static Context getContext()
    {
        return mem.getContext();
    }

    /** Get GraalVM bindings for this object */
    public static Value getGraalVMBindings()
    {
        return mem.getBindings();
    }

    @Override
    public Pair<Object, Value> call(Object contextSource, Object... argv)
    {
        Value result, thisValue;
        Object box;
        String jsName = "";

        assert(this.myReflection.canExecute());
        assert(this.myHostObject instanceof JSFunctionObjectWithMetadata);

        ((JSFunctionObjectWithMetadata) this.myHostObject).lastCallArgc = argv.length;
        ((JSFunctionObjectWithMetadata) this.myHostObject).lastCallArgv = argv;

        if (contextSource != null)
        {
            if (contextSource instanceof GraalVMSOM)
            {
                thisValue = ((GraalVMSOM) contextSource).myReflection;
                jsName = ((GraalVMSOM) contextSource).jsName;
            }
            else if (contextSource instanceof Value)
            {
                thisValue = (Value) contextSource;
                for (Entry<String, GraalVMSOM> entry : mem.entrySet())
                {
                    if (entry.getValue().myReflection.equals(thisValue))
                    {
                        jsName = entry.getValue().jsName;
                    }
                }
            }
            else
            {
                for (Entry<String, GraalVMSOM> entry : mem.entrySet())
                {
                    if (entry.getValue().myHostObject.equals(contextSource))
                    {
                        jsName = entry.getValue().jsName;
                    }
                }
                thisValue = getContext().asValue(contextSource);
            }
            if (! thisValue.isNull())
            {
                this.myReflection.putMember("this", thisValue);
            }
        }

        result = this.myReflection.execute(argv);
        box = boxReflection(result);

        // extract any members modified during call
        if ( (jsName != null) && (! jsName.isBlank()) )
        {
            thisValue = getContext().eval(RuntimeFactory.JAVASCRIPT_EXT, jsName);
        }

        return Pair.create(box, result);
    }

    @Override
    public Pair<Object, Value> callMember(String functionName, Object... argv)
    {
        return call(this.myReflection.getMember(functionName), argv);
    }

    @Override
    public void clear()
    {
        if (! this.myReflection.hasMembers())
        {
            return;
        }
        for (String key : this.myReflection.getMemberKeys())
        {
            removeMember(key);
        }
    }

    @Override
    public @NotNull Set<Entry<String, Pair<Object, Value>>> entrySet()
    {
        Map<String, Pair<Object, Value>> outrows = new HashMap<>();

        if (this.myReflection.hasMembers())
        {
            for (String key : this.myReflection.getMemberKeys())
            {
                outrows.put(key, get(key));
            }
        }

        return outrows.entrySet();
    }

    /** Note : other is assumed to be a host object (java.lang.Object.equals) */
    @Override @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    public boolean equals(Object other)
    {
        return this.myHostObject.equals(other);
    }
    // value version
    public boolean equals(Value other)
    {
        return this.myReflection.equals(other);
    }
    // mirror version
    public boolean equals(GraalVMSOM other)
    {
        return this.equals(myReflection) && this.equals((myHostObject));
    }

    @Override
    public Pair<Object, Value> eval(String s)
    {
        Value reflection = getContext().eval(RuntimeFactory.JAVASCRIPT_EXT, s);
        Object box = boxReflection(reflection);

        return Pair.create(box, reflection);
    }

    @Override
    public FrozenGraalVMSOM freeze()
    {
        return new FrozenGraalVMSOM(this);
    }

    /** Copy of this GraalVMSOM without modifiable members or properties */
    public static class FrozenGraalVMSOM implements CompatScriptObjectMirror<Pair<Object, Value>>
    {
        private final GraalVMSOM frozen;
        public FrozenGraalVMSOM(GraalVMSOM toFreeze)
        {
            this.frozen = toFreeze;
        }

        @Override public Pair<Object, Value> call(Object func, Object... args) { throw new UnsupportedOperationException("call on frozen object can impact state"); }
        @Override public Pair<Object, Value> callMember(String functionName, Object... args) { throw new UnsupportedOperationException("call on frozen object can impact state"); }
        @Override public void clear() {}
        @Override public boolean containsKey(Object key) { return frozen.containsKey(key); }
        @Override public boolean containsValue(Object value) { return frozen.containsValue(value); }
        @Override public boolean delete(Object key) { return false; }
        @Override public @NotNull Set<Entry<String, Pair<Object, Value>>> entrySet() { return frozen.entrySet(); }
        @Override public Pair<Object, Value> eval(String s) { return frozen.eval(s); }
        @Override public CompatScriptObjectMirror<Pair<Object, Value>> freeze() { return this; }
        @Override public Pair<Object, Value> get(Object key) { return frozen.get(key); }
        @Override public String getClassName() { return frozen.getClassName(); }
        @Override public Pair<Object, Value> getDefaultValue(Class<?> hint) { return frozen.getDefaultValue(hint); }
        @Override public Pair<Object, Value> getMember(String name) { return frozen.getMember(name); }
        @Override public String[] getOwnKeys(boolean all) { return frozen.getOwnKeys(all); }
        @Override public Pair<Object, Value> getOwnPropertyDescriptor(String key) { return frozen.getOwnPropertyDescriptor(key); }
        @Override public Pair<Object, Value> getProto() { return frozen.getProto(); }
        @Override public Pair<Object, Value> getSlot(int index) { return frozen.getSlot(index); }
        @Override public boolean hasMember(String name) { return frozen.hasMember(name); }
        @Override public boolean hasSlot(int slot) { return frozen.hasSlot(slot); }
        @Override public boolean isArray() { return frozen.isArray(); }
        @Override public boolean isEmpty() { return frozen.isEmpty(); }
        @Override public boolean isExtensible() { return frozen.isExtensible(); }
        @Override public boolean isFrozen() { return true; }
        @Override public boolean isFunction() { return frozen.isFunction(); }
        @Override public boolean isInstance(Object instance) { return frozen.isInstance(instance); }
        @Override public boolean isSealed() { return frozen.isSealed(); }
        @Override public boolean isStrictFunction() { return frozen.isStrictFunction(); }
        @Override public @NotNull Set<String> keySet() { return frozen.keySet(); }
        @Override public Pair<Object, Value> newObject(Object... args) { return frozen.newObject(args); }
        @Override public CompatScriptObjectMirror<Pair<Object, Value>> preventExtensions() { return frozen.preventExtensions(); }
        @Override public Pair<Object, Value> put(String key, Pair<Object, Value> value) { return null; }
        @Override public Pair<Object, Value> remove(Object key) { return null; }
        @Override public void putAll(@NotNull Map<? extends String, ? extends Pair<Object, Value>> map) { }
        @Override public void removeMember(String name) { }
        @Override public CompatScriptObjectMirror<Pair<Object, Value>> seal() { return frozen.seal(); }
        @Override public void setIndexedPropertiesToExternalArrayData(ByteBuffer buf) { }
        @Override public void setMember(String name, Pair<Object, Value> value) { }
        @Override public void setProto(Object proto) { }
        @Override public void setSlot(int index, Pair<Object, Value> value) { }
        @Override public int size() { return frozen.size(); }
        @Override public <G> G to(Class<G> type) { return frozen.to(type); }
        @Override public @NotNull Collection<Pair<Object, Value>> values() { return frozen.values(); }
    }

    @Override
    public String getClassName()
    {
        if (this.myReflection.isMetaObject())
        {
            return this.myReflection.getMetaQualifiedName();
        }
        return this.myHostObject.getClass().getCanonicalName();
    }

    /** https://262.ecma-international.org/5.1/#sec-8.12.8 */
    @Override
    public Pair<Object, Value> getDefaultValue(Class<?> hint)
    {
        if (hint == null)
        {
            return null;
        }
        if (hint == Number.class)
        {
            if (this.myReflection.hasMember("valueOf") && this.getMember("valueOf").getRight().canExecute())
            {
                return callMember("valueOf", this);
            }
            if (this.myReflection.hasMember("toString") && this.getMember("toString").getRight().canExecute())
            {
                return callMember("toString, this");
            }
        }
        if (hint == String.class)
        {
            if (this.myReflection.hasMember("toString") && this.getMember("toString").getRight().canExecute())
            {
                return callMember("toString, this");
            }
            if (this.myReflection.hasMember("valueOf") && this.getMember("valueOf").getRight().canExecute())
            {
                return callMember("valueOf", this);
            }
        }
        throw new TypeNotPresentException("Number|String", new Exception("valueOf or toString must be present"));
    }

    @Override @NotNull
    public Pair<Object, Value> getMember(String name)
    {
        Value reflection = getContext().asValue(null);
        Object box;

        if (this.myReflection.hasMember(name))
        {
            reflection = this.myReflection.getMember(name);
        }
        box = boxReflection(reflection);

        return Pair.create(box, reflection);
    }

    @Override
    public String[] getOwnKeys(boolean all)
    {
        List<String> retval = new ArrayList<>();
        Set<String> memberKeys = keySet();
        Value checkIfProperty;

        for (String key : memberKeys)
        {
            checkIfProperty = this.myReflection.getMember(key);
            if ( (all) || (! checkIfProperty.isMetaObject() && ! checkIfProperty.canExecute() && ! checkIfProperty.canInstantiate()) )
            {
                retval.add(key);
            }
        }

        return retval.toArray(String[]::new);
    }

    /** https://262.ecma-international.org/5.1/#sec-8.12.8 */
    @Override
    public Pair<Object, Value> getOwnPropertyDescriptor(String key)
    {
        Value undefinedV = RuntimeFactory.context.asValue("undefined");
        Object undefinedO = boxReflection(undefinedV);
        Pair<Object, Value> undefined = Pair.create(undefinedO, undefinedV);
        Pair<Object, Value> primitive;
        int index = 0;
        long len;
        String str, resultStr, propertyDescriptorString;
        Map<String, String> propertyDescriptor = new HashMap<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Value retvalRight;
        Object retvalLeft;

        if (! this.myReflection.isString())
        {
            return undefined;
        }
        if (this.myReflection.hasMember("[[GetOwnProperty]]") && this.getMember("[[GetOwnProperty]]").getRight().canExecute())
        {
            return callMember("[[GetOwnProperty]]", key);
        }

        try
        {
            index = Math.abs(Integer.parseInt(key));
            if (! Integer.toString(index).equals(key))
            {
                return undefined;
            }
        }
        catch (Exception ignored) { }

        if (this.myReflection.hasMember("[[PrimitiveValue]]") && this.myReflection.getMember("[[PrimitiveValue]]").canExecute())
        {
            primitive = callMember("[[PrimitiveValue]]", key);
            str = primitive.getRight().asString();
            len = str.length();
            if (len <= index)
            {
                return undefined;
            }
            resultStr = "" + str.charAt(index);
            propertyDescriptor.put("[[Value]]", resultStr);
            propertyDescriptor.put("[[Enumerable]]", "true");
            propertyDescriptor.put("[[Writeable]]", "false");
            propertyDescriptor.put("[[Configurable]]", "false");
            propertyDescriptorString = gson.toJson(propertyDescriptor);
            retvalRight = RuntimeFactory.context.asValue(propertyDescriptorString);
            retvalLeft = boxReflection(retvalRight);
            return Pair.create(retvalLeft, retvalRight);
        }

        return undefined;
    }

    @Override @Deprecated
    public Pair<Object, Value> getProto()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Pair<Object, Value> getSlot(int index)
    {
        Value right = this.myReflection.getArrayElement(index);
        Object left = boxReflection(right);

        return Pair.create(left, right);
    }

    @Override
    public boolean hasMember(String name)
    {
        return this.myReflection.hasMember(name);
    }

    @Override
    public boolean hasSlot(int slot)
    {
        long n = this.myReflection.getArraySize();
        return (0 <= slot) && (slot < n);
    }

    @Override
    public boolean isArray()
    {
        return this.myReflection.hasArrayElements();
    }

    @Override
    public boolean isEmpty()
    {
        Value proxyMemberKeys = getContext().asValue(this.myReflection.getMemberKeys());

        return ( this.myReflection.hasMembers() && this.myReflection.getMemberKeys().size() < 1 )
            || ( this.myReflection.hasArrayElements() && this.myReflection.getArraySize() < 1 )
            || ( this.myReflection.isString() && this.myReflection.asString().length() < 1 )
            || ( proxyMemberKeys.getArraySize() < 1 )
            || ( ! this.myReflection.isNull() );
    }

    @Override
    public boolean containsKey(Object key)
    {
        return this.myReflection.hasMember(key.toString());
    }

    /** Host Object version */
    @Override
    public boolean containsValue(Object value)
    {
        Set<String> memberKeys = keySet(), memKeys = mem.keySet();
        Value proxyVal;
        GraalVMSOM memEntry;

        for (String memberKey : memberKeys)
        {
            proxyVal = this.myReflection.getMember(memberKey);
            for (String memKey : memKeys)
            {
                memEntry = mem.get(memKey);
                if (memEntry.myReflection.equals(proxyVal))
                {
                    return memEntry.myHostObject.equals(value);
                }
            }
        }

        return false;
    }
    /** Reflected object version */
    public boolean containsValue(Value value)
    {
        return containsValue(boxReflection(value));
    }

    @Override
    public boolean delete(Object key)
    {
        Value vmapseed;

        // if this isn't a member, it isn't a property
        if ( ! this.myReflection.hasMember(key.toString()) )
        {
            return false;
        }

        // methods aren't properties
        vmapseed = this.myReflection.getMember(key.toString());
        if (vmapseed.isMetaObject() || vmapseed.canExecute() || vmapseed.canInstantiate())
        {
            return false;
        }

        // we have a property, delete
        removeMember(key.toString());

        return true;
    }

    @Nullable @Override
    public Pair<Object, Value> get(Object key)
    {
        Value reflection;
        Object box;

        if (this.myReflection.hasMember(key.toString()))
        {
            reflection = this.myReflection.getMember(key.toString());
            box = boxReflection(reflection);
            return Pair.create(box, reflection);
        }

        return null;
    }

    @Override
    public boolean isExtensible()
    {
        if (! this.myReflection.isProxyObject())
        {
            return false;
        }
        return getContext()
            .eval(RuntimeFactory.JAVASCRIPT_EXT, "(a) -> Object.isExtensible(a);")
            .execute(this.myReflection.asProxyObject())
            .asBoolean();
    }

    @Override
    public boolean isFrozen()
    {
        return false;
    }

    @Override
    public boolean isFunction()
    {
        return this.myReflection.canExecute();
    }

    @Override
    public boolean isInstance(Object instance)
    {
        Value mo;
        String lhs, rhs = instance.getClass().getCanonicalName();

        if (! this.myReflection.isMetaObject())
        {
            return false;
        }
        mo = this.myReflection.getMetaObject();
        lhs = mo.getMetaQualifiedName();

        return lhs.equals(rhs);
    }

    @Override
    public boolean isSealed()
    {
        return false;
    }

    @Override
    public boolean isStrictFunction()
    {
        return false;
    }

    @Override
    public @NotNull Set<String> keySet()
    {
        if (! this.myReflection.hasMembers())
        {
            return new HashSet<>();
        }
        return this.myReflection.getMemberKeys();
    }

    @Override
    public Pair<Object, Value> newObject(Object... args)
    {
        Value reflection = this.myReflection.newInstance(args);
        Object box = boxReflection(reflection);

        return Pair.create(box, reflection);
    }

    @Override
    public GraalVMSOM preventExtensions()
    {
        getContext()
            .eval(RuntimeFactory.JAVASCRIPT_EXT, "(a) -> Object.preventExtensions(a);")
            .execute(this.myReflection);

        return this;
    }

    /** Implemented for mirrors, reflections, and all viable host objects */
    @NotNull @Override
    public Pair<Object, Value> put(String key, Pair<Object, Value> value)
    {
        Value reflection = getContext().asValue(null);
        Object box;
        Pair<Object, Value> mirror;

        if (this.myReflection.hasMember(key))
        {
            reflection = this.myReflection.getMember(key);
        }

        setMember(key, value);

        box = boxReflection(reflection);
        mirror = Pair.create(box, reflection);
        return mirror;
    }
    @NotNull
    public Pair<Object, Value> put(String key, String hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, boolean hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, Number hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, LocalTime hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, Date hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, Duration hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, TimeZone hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, List<Object> hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, Exception hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, Iterator<Object> hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, JSFunctionObjectWithMetadata hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, CompatBufferElementCollection hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, Map<String, Pair<Object, Value>> hostObject)
    {
        return put(key, hostObject, false);
    }
    @NotNull
    public Pair<Object, Value> put(String key, Value reflection)
    {
        return put(key, boxReflection(reflection), true);
    }
    @NotNull
    private Pair<Object, Value> put(String key, Object hostObject, boolean ignored)
    {
        Value reflection = getContext().asValue(hostObject);
        Pair<Object, Value> mirror = Pair.create(hostObject, reflection);
        return put(key, mirror);
    }

    @Override
    public Pair<Object, Value> remove(Object key)
    {
        Value retval = getContext().asValue(null);
        Object box;

        if (this.myReflection.hasMember(key.toString()))
        {
            retval = this.myReflection.getMember(key.toString());
            removeMember(key.toString());
        }

        box = boxReflection(retval);
        return Pair.create(box, retval);
    }
    public Pair<Object, Value> remove(Value key)
    {
        return remove(boxReflection(key));
    }

    @Override
    public void putAll(@NotNull Map<? extends String, ? extends Pair<Object, Value>> map)
    {
        for (Entry<? extends String, ? extends Pair<Object, Value>> mirror : map.entrySet())
        {
            setMember(mirror.getKey(), mirror.getValue());
        }
    }

    @Override
    public void removeMember(String name)
    {
        Value toRemove;

        if (this.myReflection.hasMember(name))
        {
            this.myReflection.removeMember(name);
            if (mem.containsKey(name))
            {
                mem.remove(name);
            }
        }
    }

    @Override
    // https://262.ecma-international.org/5.1/#sec-8.12.8
    public CompatScriptObjectMirror<Pair<Object, Value>> seal()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Deprecated due to being a Nashorn-only extension
     */
    @Override @Deprecated
    public void setIndexedPropertiesToExternalArrayData(ByteBuffer buf)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setMember(String name, Pair<Object, Value> value)
    {
        this.myReflection.putMember(name, value.getRight());
    }

    /** setMember with an inferred name */
    public void setMember(Value reflection)
    {
        Object box = boxReflection(reflection);
        String name = ScriptObjectContext.getNewJSObjectName(box);
        setMember(name, Pair.create(box, reflection));
    }
    public void setMember(Object box)
    {
        Value reflection = getContext().asValue(box);
        String name = ScriptObjectContext.getNewJSObjectName(box);
        setMember(name, Pair.create(box, reflection));
    }
    public void setMember(Pair<Object, Value> mirror)
    {
        String name = ScriptObjectContext.getNewJSObjectName(mirror.getLeft());
        setMember(name, mirror);
    }

    @Override @Deprecated
    public void setProto(Object proto)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setSlot(int index, Pair<Object, Value> value)
    {
        this.myReflection.setArrayElement(index, value);
    }

    @Override
    public int size()
    {
        return keySet().size();
    }

    @Override
    public <G> G to(Class<G> type)
    {
        return this.myReflection.as(type);
    }

    @Override
    public @NotNull Collection<Pair<Object, Value>> values()
    {
        Set<Pair<Object, Value>> outrows = new HashSet<>();
        Value rowRight;
        Object rowLeft;

        for (String key : keySet())
        {
            rowRight = getContext().asValue(this.myReflection.getMember(key));
            rowLeft = boxReflection(rowRight);
            outrows.add(Pair.create(rowLeft, rowRight));
        }

        return outrows;
    }
}