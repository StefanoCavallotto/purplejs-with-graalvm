package io.purplejs.core.internal.runtime;

import io.purplejs.core.graalvm.GraalVMSOM;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;

public interface Runtime
{
    Context getContext();

    Value newJsObject();

    Value newJsArray();

    String toJsonString( Object value );

    GraalVMSOM parseJson(String value );
}