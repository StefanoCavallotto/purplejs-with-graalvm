package io.purplejs.core.internal.compat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.purplejs.core.graalvm.GraalVMSOM;
import io.purplejs.core.internal.runtime.RuntimeFactory;
import org.graalvm.collections.Pair;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Value;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InaccessibleObjectException;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Defines a consistent interface into ScriptTs between Nashorn and GraalVM
 * @param <T> either a T (deprecated) or a polyglot Value
 */
@SuppressWarnings("unused")
public interface CompatScriptObjectMirror<T>
    extends Map<String, T>
{
    /** Call this object as a JavaScript function. */
    T call(Object func, Object... args);

    /** Call member function */
    T callMember(String functionName, Object... args);

    /** Removes all of the mappings from this map (optional operation). */
    void clear();

    /** Returns true if this map contains a mapping for the specified key. */
    boolean containsKey(Object key);

    /** Returns true if this map maps one or more keys to the specified value. */
    boolean containsValue(Object value);

    /** Delete a property from this object. */
    boolean delete(Object key);

    /** Returns a Set view of the mappings contained in this map. */
    @NotNull Set<Map.Entry<String, T>> entrySet();

    /** Indicates whether some other object is "equal to" this one. */
    boolean equals(Object other);

    /** Evaluate a JavaScript expression. */
    T eval(String s);

    /** ECMA 15.2.39 - freeze implementation. */
    CompatScriptObjectMirror<T> freeze();

    /** Returns the value to which this map maps the specified key. */
    T get(Object key);

    /** ECMA [[Class]] property */
    String getClassName();

    /** Implements this object's [[DefaultValue]] method. */
    T getDefaultValue(Class<?> hint);

    /** Retrieves a named member of this JavaScript object. */
    T getMember(String name);

    /** return an array of own property keys associated with the object. */
    String[] getOwnKeys(boolean all);

    /** ECMA 8.12.1 [[GetOwnProperty]] (P) */
    T getOwnPropertyDescriptor(String key);

    /** Return the __proto__ of this object. */
    T getProto();

    /** Retrieves an indexed member of this JavaScript object. */
    T getSlot(int index);

    /** Returns a hash code value for the object. */
    int hashCode();

    /** Does this object have a named member? */
    boolean hasMember(String name);

    /** Does this object have a indexed property? */
    boolean hasSlot(int slot);

    /** Is this an array object? */
    boolean isArray();

    /** Returns true if this map contains no key-value mappings. */
    boolean isEmpty();

    /** Check if this script object is extensible */
    boolean isExtensible();

    /** Check whether this script object is frozen */
    boolean isFrozen();

    /** Is this a function object? */
    boolean isFunction();

    /** Checking whether the given object is an instance of 'this' object. */
    boolean isInstance(Object instance);

    /** Check whether this script object is sealed */
    boolean isSealed();

    /** Is this a 'use strict' function object? */
    boolean isStrictFunction();

    /** Returns the set of all property names of this object. */
    @NotNull Set<String> keySet();

    /** Call this 'constructor' JavaScript function to create a new object. */
    T newObject(Object... args);

    /** Flag this script object as non extensible */
    CompatScriptObjectMirror<T> preventExtensions();

    /** Set a named value. */
    T put(String key, T value);

    /** Adds all the mappings in a given Map to this Bindings. */
    void putAll(@NotNull Map<? extends String,? extends T> map);

    /** Removes the mapping for this key from this map if it is present (optional operation). */
    T remove(Object key);

    /** Remove a named member from this JavaScript object */
    void removeMember(String name);

    /** ECMAScript 15.2.3.8 - seal implementation */
    CompatScriptObjectMirror<T> seal();

    /** Nashorn extension: setIndexedPropertiesToExternalArrayData. */
    void setIndexedPropertiesToExternalArrayData(ByteBuffer buf);

    /** Set a named member in this JavaScript object */
    void setMember(String name, T value);

    /** Set the __proto__ of this object. */
    void setProto(Object proto);

    /** Set an indexed member in this JavaScript object */
    void setSlot(int index, T value);

    /** Returns the number of key-value mappings in this map. */
    int size();

    /** Utility to convert this script object to the given type. */
    <G> G to(Class<G> type);

    /** Returns a string representation of the object. */
    String toString();

    /** Returns the set of all property values of this object. */
    @NotNull Collection<T> values();

    /**
     * Make a script object mirror on given object if needed.
     *
     * @param on object to be wrapped/converted
     * @param homeGlobal global to which this object belongs
     */
    static GraalVMSOM wrap(Object on, GraalVMSOM homeGlobal)
    {
        Pair<Object, Value> member;
        GraalVMSOM retval = GraalVMSOM.create(on, true);

        if (homeGlobal != null)
        {
            for (String key : homeGlobal.keySet())
            {
                member = homeGlobal.getMember(key);
                if (member.getLeft() == on)
                {
                    return retval;
                }
            }

            homeGlobal.setMember(retval.myReflection);
        }

        return retval;
    }

    /**
     * Wrap an array of object to script object mirrors if needed.
     */
    static CompatScriptObjectMirror<?>[] wrapArray(Object[] args, GraalVMSOM homeGlobal)
    {
        CompatScriptObjectMirror<?>[] retval = new CompatScriptObjectMirror<?>[args.length];
        int i = 0;

        for (Object arg : args)
        {
            retval[i++] = wrap(arg, homeGlobal);
        }

        return retval;
    }

    /** Make a script object mirror on given object if needed. */
    static CompatScriptObjectMirror<?> wrapAsJSONCompatible(Object on, GraalVMSOM homeGlobal)
    {
        // Google's LinkedTreeMap is final and incompatible with custom Map types
        assert( ! (on instanceof GraalVMSOM) );
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String asJson;
        Object fromJson;

        // https://github.com/redisson/redisson/issues/2305
        try
        {
            asJson = gson.toJson(on);
            fromJson = gson.fromJson(asJson, on.getClass());
        }
        catch (InaccessibleObjectException e)
        {
            fromJson = on;
        }

        return wrap(fromJson, homeGlobal);
    }

    /** Unwrap a script object mirror if needed. */
    static Object unwrap(GraalVMSOM under, GraalVMSOM homeGlobal)
    {
        if (homeGlobal != null)
        {
            for (String key : homeGlobal.keySet())
            {
                if (new GraalVMSOM(homeGlobal.getMember(key)).equals(under))
                {
                    homeGlobal.remove(key);
                }
            }
        }

        return under.myHostObject;
    }

    /** Unwrap an array of script object mirrors if needed. */
    static Object[] unwrapArray(GraalVMSOM[] args, GraalVMSOM homeGlobal)
    {
        Object[] retval = new Object[args.length];
        int i = 0;

        for (GraalVMSOM arg : args)
        {
            retval[i++] = unwrap(arg, homeGlobal);
        }

        return retval;
    }

    /** Are the given objects mirrors to same underlying object? */
    static boolean identical(Object obj1, Object obj2)
    {
        Value vobj1 = GraalVMSOM.getContext().asValue(obj1);
        Value vobj2 = GraalVMSOM.getContext().asValue(obj2);

        return GraalVMSOM.getContext()
            .eval(RuntimeFactory.JAVASCRIPT_EXT, "“(a, b) => a === b")
            .execute(obj1, obj2)
            .asBoolean();
    }

    /** Utility to check if given object is ECMAScript undefined value */
    static boolean isUndefined(Object obj)
    {
        // if the object doesn't exist in the current context, it is undefined
        try
        {
            GraalVMSOM.getContext().asValue(obj);
        }
        catch (PolyglotException e)
        {
            return true;
        }

        // The value is defined in the current host context. Check client.
        return GraalVMSOM.getContext()
            .eval(RuntimeFactory.JAVASCRIPT_EXT, "(a) => a === undefined")
            .execute(obj)
            .asBoolean();
    }
}