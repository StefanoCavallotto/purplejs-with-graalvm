package io.purplejs.core.internal.runtime;

import com.oracle.truffle.js.scriptengine.GraalJSScriptEngine;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.Value;

public final class RuntimeFactory
{
    /** PolyGlot permittedLanguage name for JavaScript */
    public static final String JAVASCRIPT_EXT = "js";

    /** Native GraalVM runner */
    public static Context context;

    /** GraalVM bindings */
    public static Value bindings;

    /** Nashorn and GraalVM compatible engine */
    public static GraalJSScriptEngine engine;

    public Runtime newRuntime()
    {
        //noinspection unused
        Engine.Builder polyGlotEngineBuilder = null;
        Engine pgEngine = null;
        Context.Builder builder = Context
            .newBuilder(JAVASCRIPT_EXT)
            .allowHostAccess(HostAccess.ALL)
            .allowHostClassLookup(s -> true)
            .option("js.ecmascript-version", "2021");
        Value systemtype;

        //noinspection ConstantConditions
        engine = GraalJSScriptEngine.create(pgEngine, builder);
        context = engine.getPolyglotContext();
        context.enter();
        bindings = context.getBindings(JAVASCRIPT_EXT);
        systemtype = context.eval(JAVASCRIPT_EXT, "Java.type('java.lang.System')");
        bindings.putMember("system", systemtype);
        return new RuntimeImpl();
    }
}