package io.purplejs.core.internal.runtime;

import io.purplejs.core.graalvm.GraalVMSOM;
import org.graalvm.collections.Pair;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;

final class RuntimeImpl implements Runtime
{
    private final GraalVMSOM arrayProto;

    private final GraalVMSOM objectProto;

    private final GraalVMSOM jsonProto;

    RuntimeImpl()
    {
        Value reflection = getContext()
            .getBindings(RuntimeFactory.JAVASCRIPT_EXT)
            .getMember( "Array" );
        this.arrayProto = GraalVMSOM.create(reflection, "Array");

        reflection = getContext()
                .getBindings(RuntimeFactory.JAVASCRIPT_EXT)
                .getMember( "Object" );
        this.objectProto = GraalVMSOM.create(reflection, "Object");

        reflection = getContext()
                .getBindings(RuntimeFactory.JAVASCRIPT_EXT)
                .getMember( "JSON" );
        this.jsonProto = GraalVMSOM.create(reflection, "JSON");
    }

    @Override
    public Context getContext()
    {
        return Context.getCurrent();
    }

    @Override
    public Value newJsObject()
    {
        return this.objectProto.myReflection.newInstance();
    }

    @Override
    public Value newJsArray()
    {
        return this.arrayProto.myReflection.newInstance();
    }

    @Override
    public String toJsonString( final Object value )
    {
        return this.jsonProto.callMember( "stringify", value ).getRight().asString();
    }

    @Override
    public GraalVMSOM parseJson(final String value )
    {
        Pair<Object, Value> retval = this.jsonProto.callMember( "parse", value );
        return new GraalVMSOM(retval);
    }
}