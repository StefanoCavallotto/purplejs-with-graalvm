package io.purplejs.core.internal.runtime;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import io.purplejs.core.graalvm.GraalVMSOM;
import io.purplejs.core.internal.compat.CompatScriptObjectMirror;
import org.graalvm.collections.Pair;
import org.graalvm.polyglot.Value;

public final class JSHelper
{
    public static boolean isUndefined( final Object value )
    {
        return CompatScriptObjectMirror.isUndefined(value);
    }

    public static boolean isDateType( final Object value )
    {
        Value reflection = RuntimeFactory.context.asValue(value);
        return reflection.isDate();
    }

    @SuppressWarnings("unused")
    private static boolean isDateType(final GraalVMSOM value )
    {
        return value.myReflection.isDate();
    }

    public static Date toDate( final Object value )
    {
        Value reflection = RuntimeFactory.context.asValue(value);
        LocalDate ld = reflection.asDate();
        return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static boolean isArrayType( final Object value )
    {
        Value reflection = RuntimeFactory.context.asValue(value);
        return reflection.hasArrayElements();
    }

    public static boolean isObjectType( final Object value )
    {
        Value reflection = RuntimeFactory.context.asValue(value);
        return (! reflection.canExecute()) && (! reflection.canInstantiate()) ;
    }

    public static void addToObject( final Object object, final String key, final Object value )
    {
        Value reflection = RuntimeFactory.context.asValue(object);
        GraalVMSOM host = new GraalVMSOM(object, reflection);
        CompatScriptObjectMirror.wrap(value, host);
    }

    public static void addToArray( final Object array, final Object value )
    {
        Value reflection = RuntimeFactory.context.asValue(array);
        GraalVMSOM contextObj = new GraalVMSOM(array, reflection);
        Pair<Object, Value> pushMethod = contextObj.eval("push");
        String methodName = GraalVMSOM.ScriptObjectContext.getNewJSObjectName(pushMethod.getLeft());
        long size;

        assert(contextObj.isArray());
        size = contextObj.myReflection.getArraySize();
        contextObj.setMember(methodName, pushMethod);
        contextObj.callMember(methodName, value);
        assert(contextObj.myReflection.getArraySize() - size == 1);
    }
}