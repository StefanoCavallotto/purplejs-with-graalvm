package io.purplejs.core.internal.value;

import io.purplejs.core.internal.compat.CompatScriptObjectMirror;
import io.purplejs.core.internal.runtime.RuntimeFactory;
import io.purplejs.core.resource.ResourcePath;
import io.purplejs.core.value.ScriptExports;
import org.graalvm.polyglot.Value;

public final class ScriptExportsImpl
        implements ScriptExports
{
    private final ResourcePath resource;

    private final Value value;

    public ScriptExportsImpl( final ResourcePath resource, final Value value )
    {
        this.resource = resource;
        this.value = value;
    }

    @Override
    public ResourcePath getResource()
    {
        return this.resource;
    }

    @Override
    public Value getValue()
    {
        return this.value;
    }

    @Override
    public boolean hasMethod( final String name )
    {
        return getMethod( name ) != null;
    }

    @Override
    public Value executeMethod( final String name, final Object... args )
    {
        final Value method = getMethod( name );
        if ( (method == null) || (method.isNull()) )
        {
            return null;
        }

        return method.execute( args );
    }

    private Value getMethod( final String name )
    {
        final Value func = this.value.getMember( name );
        return ( (func != null) && (!CompatScriptObjectMirror.isUndefined(func)) && (! func.isNull()) && (func.canExecute()) ) ? func : null;
    }
}