package io.purplejs.core.internal.executor;

import io.purplejs.core.Environment;
import io.purplejs.core.internal.runtime.Runtime;
import io.purplejs.core.resource.ResourcePath;
import io.purplejs.core.value.ScriptExports;
import org.graalvm.polyglot.Value;

public interface ScriptExecutor
{
    Environment getEnvironment();

    ScriptExports executeMain( ResourcePath path );

    Object executeRequire( ResourcePath path );

    Runtime getNashornRuntime();

    Value newScriptValue(Object value );

    void registerMock( ResourcePath path, Object value );

    void registerDisposer( ResourcePath path, Runnable callback );
}