package io.purplejs.core.internal.executor;

import java.util.Map;
import java.util.function.Function;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.SimpleBindings;

import com.google.common.collect.Maps;

import io.purplejs.core.Environment;
import io.purplejs.core.RunMode;
import io.purplejs.core.graalvm.GraalVMSOM;
import io.purplejs.core.internal.cache.ScriptExportsCache;
import io.purplejs.core.internal.runtime.Runtime;
import io.purplejs.core.internal.runtime.RuntimeFactory;
import io.purplejs.core.internal.util.ErrorHelper;
import io.purplejs.core.internal.value.ScriptExportsImpl;
import io.purplejs.core.resource.Resource;
import io.purplejs.core.resource.ResourcePath;
import io.purplejs.core.util.IOHelper;
import io.purplejs.core.value.ScriptExports;
import org.graalvm.polyglot.Value;

public final class ScriptExecutorImpl
    implements ScriptExecutor
{
    private final static String PRE_SCRIPT = "(function(__, require, resolve, log, exports, module) { ";

    private final static String POST_SCRIPT = "\n};)";

    private Environment environment;

    private ScriptExportsCache exportsCache;

    private Map<ResourcePath, Object> mocks;

    private Map<ResourcePath, Runnable> disposers;

    private Runtime runtime;

    @Override
    public Environment getEnvironment()
    {
        return this.environment;
    }

    public void setNashornRuntime( final Runtime runtime)
    {
        this.runtime = runtime;
    }

    public void setEnvironment( final Environment environment )
    {
        this.environment = environment;
    }

    public void init()
    {
        this.mocks = Maps.newHashMap();
        this.disposers = Maps.newHashMap();
        this.exportsCache = new ScriptExportsCache();
    }

    @Override
    public ScriptExports executeMain( final ResourcePath path )
    {
        expireCacheIfNeeded();

        final Object exports = executeRequire( path );
        final Value value = newScriptValue( exports );
        return new ScriptExportsImpl( path, value );
    }

    private void expireCacheIfNeeded()
    {
        if ( RunMode.get() != RunMode.DEV )
        {
            return;
        }

        if ( this.exportsCache.isExpired() )
        {
            this.exportsCache.clear();
            runDisposers();
        }
    }

    @Override
    public Object executeRequire( final ResourcePath path )
    {
        final Object mock = this.mocks.get( path );
        if ( mock != null )
        {
            return mock;
        }

        final Object cached = this.exportsCache.get( path );
        final Resource resource = loadIfNeeded( path, cached );
        if ( resource == null )
        {
            return cached;
        }

        final Object result = requireJsOrJson( resource );
        this.exportsCache.put( resource, result );
        return result;
    }

    private Object requireJsOrJson( final Resource resource )
    {
        final String ext = resource.getPath().getExtension();
        if ( ext.equals( "json" ) )
        {
            return requireJson( resource );
        }

        return requireJs( resource );
    }

    private Object requireJs( final Resource resource )
    {
        final ResourcePath path = resource.getPath();

        final Bindings bindings = new SimpleBindings();
        bindings.put( ScriptEngine.FILENAME, path.toString() );

        final GraalVMSOM func = prepExecutable( bindings, resource );
        return executeRequire( path, func );
    }

    private Object requireJson( final Resource resource )
    {
        try
        {
            final String text = IOHelper.readString( resource.getBytes() );
            return this.runtime.parseJson( text );
        }
        catch ( final Exception e )
        {
            throw ErrorHelper.INSTANCE.handleError( e );
        }
    }

    @Override
    public Value newScriptValue( final Object value )
    {
        return RuntimeFactory.context.asValue(value);
    }

    private Resource loadIfNeeded( final ResourcePath key, final Object cached )
    {
        if ( cached == null )
        {
            return loadResource( key );
        }

        return null;
    }

    private Resource loadResource( final ResourcePath key )
    {
        return this.environment.getResourceLoader().load( key );
    }

    private Object executeRequire( final ResourcePath script, final GraalVMSOM func )
    {
        try
        {
            Value exportsValue = this.runtime.newJsObject(), moduleValue = this.runtime.newJsObject();
            Object[] args = new Object[0];
            final GraalVMSOM exports = GraalVMSOM.create(exportsValue, "exports", true, args);
            final GraalVMSOM module = GraalVMSOM.create(moduleValue, "module", true, args);
            module.put( "id", script.toString() );
            module.put( "exports", exports );

            final ExecutionContextImpl context = new ExecutionContextImpl( this, script );
            final Function<String, Object> requireFunc = context::require;
            final Function<String, ResourcePath> resolveFunc = context::resolve;

            func.call( exports, context, requireFunc, resolveFunc, context.getLogger(), exports, module );
            return module.get( "exports" );
        }
        catch ( final Exception e )
        {
            throw ErrorHelper.INSTANCE.handleError( e );
        }
    }

    private GraalVMSOM prepExecutable(final Bindings bindings, final Resource script )
    {
        try
        {
            final String text = IOHelper.readString( script.getBytes() );
            final String source = PRE_SCRIPT + text + POST_SCRIPT;
            return GraalVMSOM.prepExecutable(source);
        }
        catch ( final Exception e )
        {
            throw ErrorHelper.INSTANCE.handleError( e );
        }
    }

    @Override
    public void registerMock( final ResourcePath path, final Object value )
    {
        this.mocks.put( path, value );
    }

    @Override
    public void registerDisposer( final ResourcePath path, final Runnable callback )
    {
        this.disposers.put( path, callback );
    }

    private void runDisposers()
    {
        this.disposers.values().forEach( Runnable::run );
    }

    public void dispose()
    {
        runDisposers();
    }

    @Override
    public Runtime getNashornRuntime()
    {
        return this.runtime;
    }
}