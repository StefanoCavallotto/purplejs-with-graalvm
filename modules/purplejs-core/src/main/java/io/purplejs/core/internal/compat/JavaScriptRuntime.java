package io.purplejs.core.internal.compat;

import javax.script.ScriptEngine;

public interface JavaScriptRuntime<T>
{
    ScriptEngine getEngine();

    T newJsObject();

    T newJsArray();

    String toJsonString( Object value );

    T parseJson( String value );
}