package io.purplejs.http.internal.websocket;

import java.util.Set;

import io.purplejs.http.websocket.WebSocketConfig;
import org.graalvm.polyglot.Value;

final class WebSocketConfigImpl
    implements WebSocketConfig
{
    String group;

    long timeout;

    Set<String> subProtocols;

    Value attributes;

    @Override
    public String getGroup()
    {
        return this.group;
    }

    @Override
    public long getTimeout()
    {
        return this.timeout;
    }

    @Override
    public Set<String> getSubProtocols()
    {
        return this.subProtocols;
    }

    @Override
    public Value getAttributes()
    {
        return this.attributes;
    }
}
