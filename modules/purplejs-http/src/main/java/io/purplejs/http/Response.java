package io.purplejs.http;

import java.util.Map;

import com.google.common.io.ByteSource;
import com.google.common.net.MediaType;

import io.purplejs.http.websocket.WebSocketConfig;
import org.graalvm.polyglot.Value;

public interface Response
{
    Status getStatus();

    ByteSource getBody();

    @SuppressWarnings("UnstableApiUsage")
    MediaType getContentType();

    Headers getHeaders();

    Map<String, Cookie> getCookies();

    WebSocketConfig getWebSocket();

    Value getValue();
}
