package io.purplejs.http.internal.websocket;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.google.common.collect.Sets;

import io.purplejs.http.websocket.WebSocketConfig;
import org.graalvm.polyglot.Value;

public final class WebSocketConfigFactory
{
    private final static long DEFAULT_TIMEOUT = TimeUnit.SECONDS.toMillis( 30 );

    public WebSocketConfig create( final Value value )
    {
        if ( value == null )
        {
            return null;
        }

        final WebSocketConfigImpl config = new WebSocketConfigImpl();
        config.subProtocols = parseSubProtocols( value.getMember( "subProtocols" ) );
        config.attributes = value.getMember( "attributes" );
        config.group = getString( value.getMember( "group" ));
        config.timeout = getLong( value.getMember( "timeout" ));
        return config;
    }

    private Set<String> parseSubProtocols( final Value value )
    {
        final Set<String> set = Sets.newHashSet();
        List<Object> valueMembers;

        if ( value == null )
        {
            return set;
        }

        if ( ( value.isNumber() ) )
        {
            set.add( value.asString() );
        }
        else if ( value.hasArrayElements() )
        {
            valueMembers = Arrays.asList(value.as(Object[].class));
            set.addAll(
                    valueMembers.stream().
                    map(Object::toString).
                    collect( Collectors.toList() )
                );
        }

        return set;
    }

    private String getString(final Value value)
    {
        return ( ! value.isNull() ) ? value.asString() : "default";
    }

    private Long getLong(final Value value)
    {
        if ( ( ! value.isNull() ) && ( value.isNumber() ) )
        {
            try
            {
                return value.asLong();
            }
            catch ( final Exception e )
            {
                return WebSocketConfigFactory.DEFAULT_TIMEOUT;
            }
        }

        return WebSocketConfigFactory.DEFAULT_TIMEOUT;
    }
}
