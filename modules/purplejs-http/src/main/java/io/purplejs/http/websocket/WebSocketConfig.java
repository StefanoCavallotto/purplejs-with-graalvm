package io.purplejs.http.websocket;

import org.graalvm.polyglot.Value;

import java.util.Set;

public interface WebSocketConfig
{
    String getGroup();

    long getTimeout();

    Set<String> getSubProtocols();

    Value getAttributes();
}
