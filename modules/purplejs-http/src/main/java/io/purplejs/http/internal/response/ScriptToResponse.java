package io.purplejs.http.internal.response;

import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;

import io.purplejs.core.internal.compat.CompatScriptObjectMirror;
import io.purplejs.http.Cookie;
import io.purplejs.http.Response;
import io.purplejs.http.ResponseBuilder;
import io.purplejs.http.Status;
import io.purplejs.http.internal.websocket.WebSocketConfigFactory;
import org.graalvm.polyglot.Value;

public final class ScriptToResponse
{
    public Response toResponse( final Value value )
    {
        final ResponseBuilder builder = ResponseBuilder.newBuilder();
        if ( value == null || CompatScriptObjectMirror.isUndefined(value) || value.isNull() )
        {
            //noinspection UnstableApiUsage
            builder.contentType( MediaType.PLAIN_TEXT_UTF_8 );
            return builder.build();
        }

        builder.value( value );
        populateStatus( builder, value.getMember( "status" ) );

        final Value body = value.getMember( "body" );
        //noinspection UnstableApiUsage
        final MediaType type = findContentType( value.getMember( "contentType" ), body );
        populateContentType( builder, type );

        populateBody( builder, body );
        populateHeaders( builder, value.getMember( "headers" ) );
        populateCookies( builder, value.getMember( "cookies" ) );
        setRedirect( builder, value.getMember( "redirect" ) );
        setWebSocket( builder, value.getMember( "webSocket" ) );

        return builder.build();
    }

    @SuppressWarnings("UnstableApiUsage")
    private MediaType findContentType(final Value value, final Value body )
    {
        final String type = ( value != null ) ? value.as( String.class ) : null;
        if ( type != null )
        {
            return MediaType.parse( type );
        }

        return new BodySerializer().findType( body );
    }

    private void populateStatus( final ResponseBuilder builder, final Value value )
    {
        final Integer status = ( value != null ) ? value.as( Integer.class ) : null;
        builder.status( status != null ? Status.from( status ) : Status.OK );
    }

    @SuppressWarnings("UnstableApiUsage")
    private void populateContentType(final ResponseBuilder builder, final MediaType type )
    {
        builder.contentType( type != null ? type : MediaType.PLAIN_TEXT_UTF_8 );
    }

    private void populateBody( final ResponseBuilder builder, final Value value )
    {
        builder.body( new BodySerializer().toBody( value ) );
    }

    private void populateHeaders( final ResponseBuilder builder, final Value value )
    {
        if ( value == null )
        {
            return;
        }

        if ( !value.hasMembers() )
        {
            return;
        }

        for ( final String key : value.getMemberKeys() )
        {
            builder.header( key, value.getMember( key ).as( String.class ) );
        }
    }

    private void setRedirect( final ResponseBuilder builder, final Value value )
    {
        final String redirect = ( value != null ) ? value.as( String.class ) : null;
        if ( redirect == null )
        {
            return;
        }

        builder.status( Status.SEE_OTHER );
        builder.header( HttpHeaders.LOCATION, redirect );
    }

    private void populateCookies( final ResponseBuilder builder, final Value value )
    {
        if ( value == null )
        {
            return;
        }

        for ( final String key : value.getMemberKeys() )
        {
            addCookie( builder, value.getMember( key ), key );
        }
    }

    static <T> T getValue( final Value value, final Class<T> type, final T defValue )
    {
        if ( value == null )
        {
            return null;
        }

        final T result = value.as( type );
        return result != null ? result : defValue;
    }

    private <T> T getMemberValue( final Value value, final String name, final Class<T> type, final T defValue )
    {
        final T result = getValue( value.getMember( name ), type, defValue );
        return result != null ? result : defValue;
    }

    private Cookie newCookie( final Value value, final String key )
    {
        final Cookie cookie = new Cookie( key );

        if ( value.hasMembers() )
        {
            cookie.setValue( getMemberValue( value, "value", String.class, "" ) );
            cookie.setPath( getMemberValue( value, "path", String.class, null ) );
            cookie.setDomain( getMemberValue( value, "domain", String.class, null ) );
            cookie.setComment( getMemberValue( value, "comment", String.class, null ) );
            cookie.setMaxAge( getMemberValue( value, "maxAge", Integer.class, -1 ) );
            cookie.setSecure( getMemberValue( value, "secure", Boolean.class, false ) );
            cookie.setHttpOnly( getMemberValue( value, "httpOnly", Boolean.class, false ) );
        }
        else
        {
            cookie.setValue( getValue( value, String.class, "" ) );
        }

        return cookie;
    }

    private void addCookie(final ResponseBuilder builder, final Value value, final String key )
    {
        if ( value != null )
        {
            builder.cookie( newCookie( value, key ) );
        }
    }

    private void setWebSocket( final ResponseBuilder builder, final Value value )
    {
        builder.webSocket( new WebSocketConfigFactory().create( value ) );
    }
}