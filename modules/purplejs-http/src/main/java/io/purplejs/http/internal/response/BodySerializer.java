package io.purplejs.http.internal.response;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import com.google.common.net.MediaType;

import io.purplejs.core.graalvm.GraalVMSOM;
import io.purplejs.core.internal.compat.CompatScriptObjectMirror;
import io.purplejs.core.resource.Resource;
import org.graalvm.polyglot.Value;

final class BodySerializer
{
    @SuppressWarnings("UnstableApiUsage")
    private final static MediaType JSON_TYPE = MediaType.create( "application", "json" );

    ByteSource toBody( final Object value )
    {
        if ( value == null )
        {
            return null;
        }

        if ( value instanceof Resource )
        {
            return ( (Resource) value ).getBytes();
        }

        if ( value instanceof ByteSource )
        {
            return (ByteSource) value;
        }

        if ( value instanceof byte[] )
        {
            return ByteSource.wrap( (byte[]) value );
        }

        if ( value instanceof Value )
        {
            return toBody( (Value) value );
        }

        return toBody( value.toString() );
    }

    private ByteSource toBody( final Value value )
    {
        if ( value.canInstantiate() )
        {
            return toBody( value.newInstance() );
        }

        return toBody(CompatScriptObjectMirror.wrapAsJSONCompatible(GraalVMSOM.boxReflection(value), null));
    }

    private ByteSource toBody( final String value )
    {
        return ByteSource.wrap( value.getBytes( Charsets.UTF_8 ) );
    }

    @SuppressWarnings("UnstableApiUsage")
    MediaType findType(final Value value )
    {
        if ( value == null )
        {
            return null;
        }

        if ( value.hasArrayElements() || value.hasMembers() )
        {
            return JSON_TYPE;
        }

        return null;
    }
}