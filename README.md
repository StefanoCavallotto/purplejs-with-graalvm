<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<div align="center">
![Purple Logo](/htdocs/img/purplelogo.png "Purple Logo") <h1>With</h1> ![GraalVM Logo](/htdocs/img/GraalVM.png "GraalVM Logo")
</div>
<div>

<strong>PurpleJS Framework With GraalVM</strong>
--------

- [PurpleJS source](https://github.com/purplejs)
- [GraalVM source](https://github.com/graalvm)


PurpleJS is a simple and capable framework for creating performant web applications without leaving Javascript. It is created in Java to give the flexibility and performance Java provides. It's optional to use Java but it's required if you need to embed PurpleJS in existing applications.

GraalVM is a high performance runtime for Java, JavaScript, LLVM-based languages such as C and C++, and other dynamic languages. Additionally, GraalVM allows efficient interoperability between programming languages and compiling Java applications ahead-of-time into native executables for faster startup time and lower memory overhead.

Together, these combine to create a powerful JavaScript engine, enabling Java programmers to run most NodeJS scripts unmodified from Java applications.

Building
--------

This project is a checkout of PurpleJS, to which GraalVM has been added. Purple's build system is used to build this project.

PurpleJS builds with [Gradle](http://gradle.org). It is recommended you use a sysystem-wide gradle installation, but gradle wrapper 5.1.1 has been provided.

<strong>Ensuring correct Java level</strong>

PurpleJS was originally written against Java 8. Since then, support for OpenJDK 11 has been added. To endure you are using the correct JDK, you can use this bash script to remove all other JDKs from your path and replace them by the one tied to your JAVA\_HOME.

```
JAVA_HOME=/usr/share/java/jdk-11.0.2
PATH=`echo "${PATH}" |  awk -v RS=: -v ORS=: '/jdk/ {next} {print}'`
PATH="${PATH}:${JAVA_HOME}/bin"
```

<strong>Using bundled gradle</strong>

```
./gradlew build
```

<strong>Using system-wide gradle</strong>
```
gradle wrapper --gradle-version=5.1.1 build
```


Documentation
-------------

- [PurpleJS Wiki](https://github.com/purplejs/purplejs/wiki)
- [GraalVM Support Links](https://github.com/oracle/graal/#repository-structure)


Contributing
------------

Please contact jessica![.](/htdocs/img/period-punctuation-small.png "Period")d![.](/htdocs/img/period-punctuation-small.png "Period")pennell![@](/htdocs/img/atsign-small.png "At Sign")xcelenergy![.](/htdocs/img/period-punctuation-small.png "Period")com or jessicadharmapennell![@](/htdocs/img/atsign-small.png "At Sign")gmail![.](/htdocs/img/period-punctuation-small.png "Period")com

License
-------
PurpleJS With GraalVM is distributed under the MIT license


```
Copyright (c) 2021 "Jessica Pennell" &lt;jessicadharmapennell&#64;gmail.com&gt;

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

PurpleJS is distributed under the [Apache 2 License](https://opensource.org/licenses/Apache-2.0)

GraalVM is distributed under [version 2 of the GNU General Public License with the �Classpath� Exception](https://openjdk.java.net/legal/gplv2+ce.html)
</div>
